/* HandshakeMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;

import com.damfav.vcpoker.hand.Card;

/**
 * Model message to join a game
 */
public class HandshakeMessage extends AbstractMessage {

	/**
	 * true if the user connected successfully
	 */
	private boolean success;

	/**
	 * The player set in the table
	 */
	private int ownPlayerSeat;

	/**
	 * The alyeer current money
	 */
	protected double playerCurrentMoney;

	/**
	 * The money in the table
	 */
	private double moneyInTheTable;

	/**
	 * The cards in the table
	 */
	private List<Card> currentCardsInTheTable;

	/**
	 * The current turn player
	 */
	private int playerCurrentTurn;

	/**
	 * Default constructor
	 */
	public HandshakeMessage() {
	}

	public HandshakeMessage(boolean success, int ownPlayerSeat,
			double playerCurrentMoney, double moneyInTheTable,
			List<Card> currentCardsInTheTable, int playerCurrentTurn) {
		super();
		this.success = success;
		this.ownPlayerSeat = ownPlayerSeat;
		this.playerCurrentMoney = playerCurrentMoney;
		this.moneyInTheTable = moneyInTheTable;
		this.currentCardsInTheTable = currentCardsInTheTable;
		this.playerCurrentTurn = playerCurrentTurn;
	}

	// GETTERS & SETTERS
	public double getPlayerCurrentMoney() {
		return playerCurrentMoney;
	}

	public void setPlayerCurrentMoney(double playerCurrentMoney) {
		this.playerCurrentMoney = playerCurrentMoney;
	}

	public int getOwnPlayerSeat() {
		return ownPlayerSeat;
	}

	public void setOwnPlayerSeat(int ownPlayerSeat) {
		this.ownPlayerSeat = ownPlayerSeat;
	}

	@Override
	public int getCode() {
		return MessageFactory.MSG_HANDSHAKE;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public double getMoneyInTheTable() {
		return moneyInTheTable;
	}

	public void setMoneyInTheTable(double moneyInTheTable) {
		this.moneyInTheTable = moneyInTheTable;
	}

	public List<Card> getCurrentCardsInTheTable() {
		return currentCardsInTheTable;
	}

	public void setCurrentCardsInTheTable(List<Card> currentCardsInTheTable) {
		this.currentCardsInTheTable = currentCardsInTheTable;
	}

	public int getPlayerCurrentTurn() {
		return playerCurrentTurn;
	}

	public void setPlayerCurrentTurn(int playerCurrentTurn) {
		this.playerCurrentTurn = playerCurrentTurn;
	}

}
