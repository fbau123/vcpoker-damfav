/* ChangePlayerViewMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

/**
 * Message model to change the player view
 */
public abstract class ChangePlayerViewMessage extends AbstractMessage {

	/**
	 * Money to put into the table
	 */
	private double moneyToPutInTheTable;

	/**
	 * money to set inthe user current money
	 */
	private double moneyToSetInTheUserCurrentMoney;

	/**
	 * money to set in user bid
	 */
	private double moneyToSetInUserBid;

	public ChangePlayerViewMessage() {
	}

	public ChangePlayerViewMessage(double moneyToPutInTheTable,
			double moneyToSetInTheUserCurrentMoney, double moneyToSetInUserBid) {
		super();
		this.moneyToPutInTheTable = moneyToPutInTheTable;
		this.moneyToSetInTheUserCurrentMoney = moneyToSetInTheUserCurrentMoney;
		this.moneyToSetInUserBid = moneyToSetInUserBid;
	}

	// GETTERS & SETTERS
	public double getMoneyToPutInTheTable() {
		return moneyToPutInTheTable;
	}

	public void setMoneyToPutInTheTable(double moneyToPutInTheTable) {
		this.moneyToPutInTheTable = moneyToPutInTheTable;
	}

	public double getMoneyToSetInTheUserCurrentMoney() {
		return moneyToSetInTheUserCurrentMoney;
	}

	public void setMoneyToSetInTheUserCurrentMoney(
			double moneyToSetInTheUserCurrentMoney) {
		this.moneyToSetInTheUserCurrentMoney = moneyToSetInTheUserCurrentMoney;
	}

	public double getMoneyToSetInUserBid() {
		return moneyToSetInUserBid;
	}

	public void setMoneyToSetInUserBid(double moneyToSetInUserBid) {
		this.moneyToSetInUserBid = moneyToSetInUserBid;
	}

}
