<%@ tag language="java" pageEncoding="US-ASCII"%>

<%@attribute name="id" required="true" %>
<%@attribute name="title" required="true" fragment="true" %>
<%@attribute name="body" fragment="true" required="true"%>
<%@attribute name="footer" fragment="true" required="true"%>

<div id="${id}" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><jsp:invoke fragment="title" /></h4>
      </div>
      <div class="modal-body">
		<jsp:invoke fragment="body" />
      </div>
      <div class="modal-footer">
		<jsp:invoke fragment="footer" />
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->