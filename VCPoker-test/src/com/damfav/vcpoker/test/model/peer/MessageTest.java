package com.damfav.vcpoker.test.model.peer;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import com.damfav.vcpoker.model.currency.Currency;
import com.damfav.vcpoker.model.peer.message.Message;
import com.damfav.vcpoker.model.peer.message.MessageFactory;
import com.damfav.vcpoker.model.peer.message.NewPeerMessage;

import android.test.AndroidTestCase;
import android.util.Log;

public class MessageTest extends AndroidTestCase {

	
	public void testNewPeerMessage(){
		
		List<String> ips = new ArrayList<String>();
		ips.add("124.12.12.12");
		ips.add("144.12.12.12");
		
		Currency bidMoney = new Currency("game", 12);
		Currency currentMoney = new Currency("game", 15);
		
		
		String username = "ddd";
		
		Message msg = new NewPeerMessage(ips, username, bidMoney, currentMoney);
		
		String json = msg.toJson();
		Log.d("jsonMESSAGE", msg.toJson());
		
		Message parsed = MessageFactory.create(json);
		
		Assert.assertTrue(parsed.getClass() == msg.getClass());
	}
}
