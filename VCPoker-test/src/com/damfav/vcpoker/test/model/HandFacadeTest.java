package com.damfav.vcpoker.test.model;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import com.damfav.vcpoker.model.hand.*;

import android.test.AndroidTestCase;
import android.util.Log;

public class HandFacadeTest extends AndroidTestCase{

	HandFacade hfacade = new HandFacade();
	
	public void testGetHandTwoPair(){
		List<Card> cards = new ArrayList<Card>();
//		cards.add(new Card(1,1));
		cards.add(new Card(1,2));
		cards.add(new Card(1,3));
//		cards.add(new Card(1,3));
		cards.add(new Card(2,1));
		cards.add(new Card(2,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == TwoPair.class);
		
	}
	
	public void testGetHandFullHouse(){
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(1,1));
		cards.add(new Card(1,2));
		cards.add(new Card(2,3));
//		cards.add(new Card(1,3));
		cards.add(new Card(2,1));
		cards.add(new Card(2,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == FullHouse.class);
		
	}
	public void testGetHandThreeOfAKind(){
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(1,1));
		cards.add(new Card(1,2));
		cards.add(new Card(1,3));
//		cards.add(new Card(1,3));
		cards.add(new Card(5,1));
		cards.add(new Card(2,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == ThreeOfAKind.class);
		
	}
	
	public void testGetHandOnePair(){
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(6,1));
		cards.add(new Card(1,2));
		cards.add(new Card(5,3));
//		cards.add(new Card(1,3));
		cards.add(new Card(2,1));
		cards.add(new Card(2,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == OnePair.class);
		
	}
	
	public void testGetHandStraight(){
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(1,1));
		cards.add(new Card(2,2));
		cards.add(new Card(3,3));
//		cards.add(new Card(1,3));
		cards.add(new Card(4,1));
		cards.add(new Card(5,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == Straight.class);
		
	}
	
	
	public void testGetHandFlush(){
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(1,2));
		cards.add(new Card(2,2));
		cards.add(new Card(7,2));
//		cards.add(new Card(1,3));
		cards.add(new Card(4,2));
		cards.add(new Card(5,3));
		Hand output = hfacade.getHand(cards);
		Log.d("class", output.getClass().getName());
		Assert.assertTrue(output.getClass() == Flush.class);
		
	}
	
	
	
	public void testBestHand7Cards(){
		List<Hand> hands = new ArrayList<Hand>();
		
		List<Card> thCards = new ArrayList<>();
		thCards.add(new Card(1,1));
		thCards.add(new Card(1,2));
		thCards.add(new Card(3,2));
		thCards.add(new Card(1,5));
		thCards.add(new Card(5,6));
		thCards.add(new Card(8,6));
		thCards.add(new Card(9,6));
		Hand three = new ThreeOfAKind(thCards);
		hands.add(three);
		
		List<Card> fullCards = new ArrayList<>();
		thCards.add(new Card(1,1));
		thCards.add(new Card(1,2));
		thCards.add(new Card(3,2));
		thCards.add(new Card(1,5));
		thCards.add(new Card(3,6));
		thCards.add(new Card(8,6));
		thCards.add(new Card(9,6));
		Hand full = new FullHouse(fullCards);
		hands.add(full);
		
		List<Card> twoCards = new ArrayList<>();
		thCards.add(new Card(1,1));
		thCards.add(new Card(1,2));
		thCards.add(new Card(3,2));
		thCards.add(new Card(3,5));
		thCards.add(new Card(5,6));
		thCards.add(new Card(8,6));
		thCards.add(new Card(9,6));
		Hand two = new TwoPair(twoCards);
		hands.add(two);
		
		
		hfacade.findBestHand(hands).equals(full);
	}
}
