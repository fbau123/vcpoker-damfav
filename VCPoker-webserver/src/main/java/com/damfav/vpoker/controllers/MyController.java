package com.damfav.vpoker.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;

public abstract class MyController {
	
	@Autowired
	protected Validator validator;
	
	public String getViewsDir(){
		return getClass()
				.getSimpleName()
				.split("Controller")[0]
				.toLowerCase();
	}
	
	public String view(String viewName){
		return getViewsDir() + "/" + viewName;
	}
	
	
}
