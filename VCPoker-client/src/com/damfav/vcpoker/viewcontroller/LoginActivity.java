/* LoginActivity.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.damfav.vcpoker_client.R;

/**
 * The Game login view
 */
public class LoginActivity extends Activity implements OnClickListener {

	/**
	 * Shared preference instance to save information and recover the
	 * information
	 */
	private SharedPreferences mPrefs;

	/**
	 * The login dialog
	 */
	private Dialog loginDialog;

	/**
	 * This field is used to tag the debug message in the logcat
	 */
	private static final String TAG = "LoginActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// set the activity_main to the Mainactivity view
		setContentView(R.layout.activity_login);

		// set all the onclicklisteners on the buttons
		setListeners();

		// get the application shared preferences
		mPrefs = getSharedPreferences(Constants.LOGINPREFERENCEFILE,
				MODE_PRIVATE);

		// get the preference login
		String currentLogin = mPrefs
				.getString(Constants.LOGINPREFERENCEKEY, "");

		// if exists it exists and is different then the "" then go to next
		// application
		if (currentLogin != null && !currentLogin.equals("")) {
			goToNextActivity(currentLogin);
		}

	}

	/**
	 * This method put all the listeners in the buttons
	 */
	private void setListeners() {

		// Find each login type button and put a listener on it
		findViewById(R.id.activity_main_btn_guestLogin)
				.setOnClickListener(this);
		findViewById(R.id.activity_main_btn_appLogin).setOnClickListener(this);
		findViewById(R.id.activity_main_btn_FBLogin).setOnClickListener(this);
		findViewById(R.id.activity_main_btn_googleLogin).setOnClickListener(
				this);

	}

	@Override
	public void onClick(View v) {
		// switch the vies id
		switch (v.getId()) {
		case R.id.activity_main_btn_guestLogin:
			// if the button is the guest button go to next activity like guest
			goToNextActivity(Constants.GUESTLOGIN);

			break;
		case R.id.activity_main_btn_appLogin:
			// if the login dialog is been showing
			if (loginDialog != null) {
				// dismiss the logindialog to create another one
				loginDialog.dismiss();
			}

			// create a new login Dialog
			loginDialog = new Dialog(this);
			loginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			// set the content of the dialog with the created xml view
			loginDialog.setContentView(R.layout.apploginlayout);

			// but the listeners in the buttons
			loginDialog.findViewById(R.id.apploginlayout_btn_forgetPass)
					.setOnClickListener(this);
			loginDialog.findViewById(R.id.apploginlayout_btn_login)
					.setOnClickListener(this);
			loginDialog.findViewById(R.id.apploginlayout_btn_signUp)
					.setOnClickListener(this);

			// show the logina dialog
			loginDialog.show();

			break;
		case R.id.activity_main_btn_FBLogin:
			// When the user select the Facebook login, get the mail from the
			// facebook account
			getMailFromFB();

			break;
		case R.id.activity_main_btn_googleLogin:

			Toast.makeText(this, "Working...", Toast.LENGTH_SHORT).show();

			break;
		case R.id.apploginlayout_btn_forgetPass:
			// TODO implement the forget password option
			Toast.makeText(this, "Implementation still pending",
					Toast.LENGTH_SHORT).show();
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://www.google.com"));
			startActivity(browserIntent);

			break;
		case R.id.apploginlayout_btn_login:
			// when the user pressed the login button get the user Email and
			// password
			String loginEmail = ((EditText) loginDialog
					.findViewById(R.id.apploginlayout_et_email)).getText()
					.toString();
			String loginPassword = ((EditText) loginDialog
					.findViewById(R.id.apploginlayout_et_password)).getText()
					.toString();

			// login the user with the getter user information
			loginUser(loginEmail, loginPassword);

			break;
		case R.id.apploginlayout_btn_signUp:
			// When the user select sign up option in the login dialog

			// dismiss the old login dialog with the email and password
			loginDialog.dismiss();

			// create a new dialog
			loginDialog = new Dialog(this);
			loginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			// set the content of the dialog with the sign up xml layout
			loginDialog.setContentView(R.layout.sign_up_layout);

			// put the listeners in the create and account buttons
			loginDialog.findViewById(R.id.sign_up_layout_btn_create)
					.setOnClickListener(this);
			loginDialog.findViewById(R.id.sign_up_layout_btn_account)
					.setOnClickListener(this);

			// show the dialog to the user
			loginDialog.show();
			break;
		case R.id.sign_up_layout_btn_create:
			// when the user select to create the account

			// get all the information from the edittexts in the dialog
			String email = ((EditText) loginDialog
					.findViewById(R.id.sign_up_layout_et_email)).getText()
					.toString();
			String password = ((EditText) loginDialog
					.findViewById(R.id.sign_up_layout_et_password)).getText()
					.toString();
			String confirmPassword = ((EditText) loginDialog
					.findViewById(R.id.sign_up_layout_et_confirmPassword))
					.getText().toString();

			// check if the both passwords are the same
			if (!password.equals(confirmPassword)) {
				// if the password doesen't match tell to the user about the
				// problem
				Toast.makeText(this,
						"The password doesn't match Please try again",
						Toast.LENGTH_SHORT).show();
			} else {
				// otherwise register the user
				registerUser(email, password, confirmPassword);
			}
			break;
		case R.id.sign_up_layout_btn_account:

			// when the user select the account button in the signup dialog,
			// return the user to the login dialog
			this.onClick(findViewById(R.id.activity_main_btn_appLogin));
			break;

		}

	}

	/**
	 * Gets user email from Facebook account.
	 */
	private void getMailFromFB() {
		// TODO implementation still pending
	}

	/**
	 * Intents to next activity after Login.
	 * 
	 * @param loginType
	 */
	private void goToNextActivity(String loginType) {

		// create the intent for the activity ProfileActivity
		Intent i = new Intent(getApplicationContext(), ProfileActivity.class);

		// put the login type in the sharedpreferences
		mPrefs.edit().putString(Constants.LOGINPREFERENCEKEY, loginType)
				.commit();
		Log.e("EMAIL",
				"EMAIL IS "
						+ mPrefs.getString(Constants.LOGINPREFERENCEKEY, "no"));
		Log.d(TAG,
				"Entrying in gotonextActivity: "
						+ mPrefs.getString(Constants.LOGINPREFERENCEKEY, "A"));

		// start the next Activity
		startActivity(i);
	}

	/**
	 * Logs user in.
	 * 
	 * @param email, user's email address
	 * @param password, user's password
	 */
	private void loginUser(String email, String password) {

		// Create a new server access task
		ServerAccessTask rt = new ServerAccessTask();

		// create the parameters with login in the server
		String[] paramsToLogin = { "" + Constants.LOGINTYPE, email, password };

		// execute the ServerAccessTask
		rt.execute(paramsToLogin);

	}

	/**
	 * Registers user in a VCPoker account.
	 * 
	 * @param email
	 * @param password
	 * @param confirmPassword
	 */
	private void registerUser(String email, String password,
			String confirmPassword) {

		// Create a new server access task
		ServerAccessTask rt = new ServerAccessTask();

		// create a array with the information needed to register the user
		String[] paramsToRegister = { "" + Constants.REGISTRATIONTYPE, email,
				password, confirmPassword };

		// execute the server access task
		rt.execute(paramsToRegister);

	}

	/**
	 * This {@link AsyncTask} is created to access to server to login or
	 * register the user
	 * 
	 * @author Awais Iqbal
	 * 
	 */
	private class ServerAccessTask extends AsyncTask<String, Void, Integer> {
		/**
		 * This filed is to save the connection type depending if is the
		 * registration task or login task
		 */
		private String connectionType;

		/**
		 * In this field has to save the user email
		 */
		String email = "";

		boolean guest = false;

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			this.dialog = ProgressDialog.show(LoginActivity.this, "Loading...",
					"Getting your profile...", true, false);
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(String... params) {
			// get the information from the parameter array
			connectionType = params[0];
			email = params[1];
			String password = params[2];
			if (email == null && password == null) {
				guest = true;
			}

			// create a new string to save the confirmation password
			String confirmPassword = "";
			// target url where to connect to connect
			String targetURL;

			if (connectionType.equals("" + Constants.REGISTRATIONTYPE)) {
				// if the connection type is registration get the confirm
				// password
				confirmPassword = params[3];

				// set the target url
				targetURL = "http://damfav-vpoker.rhcloud.com/user/signUp";
			} else {
				// if the connection type is login just set the target url
				targetURL = "http://damfav-vpoker.rhcloud.com/user/login";
			}

			// create a post connection to the server
			HttpPost post = new HttpPost(targetURL);
			Header header = new BasicHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)");
			post.setHeader(header);

			// create a client http connection to execute the post request
			HttpClient client = new DefaultHttpClient();

			// create a new response code
			int resCode = 0;
			try {
				// create a list of NameValuePair with the user information to
				// send to the server
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				if (!guest) {
					nameValuePairs.add(new BasicNameValuePair("email", email));
					nameValuePairs.add(new BasicNameValuePair("password",
							password));
					// if the connection type is registration
					if (this.connectionType.equals(Constants.REGISTRATIONTYPE)) {
						// add the cofirm password to the list of parameters to
						// send
						nameValuePairs.add(new BasicNameValuePair(
								"confirmPassword", confirmPassword));
					}
				}

				// set the list in the post request
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// execute the client with the post request and get the response
				HttpResponse response = client.execute(post);

				// get the response code from the server
				resCode = response.getStatusLine().getStatusCode();

			} catch (IOException e) {
				e.printStackTrace();
			}

			// return the response code from ther server
			return resCode;
		}

		@Override
		protected void onPostExecute(Integer result) {
			this.dialog.dismiss();
			// switch the response code getted from the server
			switch (result.intValue()) {
			// is the
			case Constants.OK:
				// is everything ok dismiss the login dialog
				LoginActivity.this.loginDialog.dismiss();

				if (connectionType.equals(Constants.REGISTRATIONTYPE)) {
					// if the connection was the registration toast to the user
					// showing about the account successfully created
					Toast.makeText(LoginActivity.this,
							"Registered successfully!", Toast.LENGTH_SHORT)
							.show();
				} else {
					// other wise just save the connected email and go to the
					// next activity like APP login
					LoginActivity.this.mPrefs.edit()
							.putString(Constants.LOGGEDEMAIL, this.email)
							.commit();
					LoginActivity.this.goToNextActivity(Constants.APPLOGIN);
				}

				break;
			case Constants.EMAILWRONGFORMAT:
				// create a toast showing the error
				Toast.makeText(LoginActivity.this,
						"Email Address wrong format", Toast.LENGTH_LONG).show();
				break;
			case Constants.WRONGPASSWORD:
				// create a toast showing the error
				Toast.makeText(LoginActivity.this, "Wrong password",
						Toast.LENGTH_LONG).show();
				break;
			case Constants.ERROR:
				// create a toast showing the error
				Toast.makeText(LoginActivity.this, "Connection Error",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		protected void onCancelled() {
			// when the server access is cancelled tell to the user about the
			// cancellation
			Toast.makeText(LoginActivity.this, "Canceled!", Toast.LENGTH_SHORT)
					.show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

}
