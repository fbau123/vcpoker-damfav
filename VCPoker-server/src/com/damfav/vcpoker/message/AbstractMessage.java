 /* AbstractMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import com.google.gson.Gson;

/**
 * The base class for all the Message. This class implements the
 * basic logic to convert a Message to json format. Subclasses will
 * have to implement the method concreteToJson which takes the JSONObject
 * just before converting it to String, so each subclass can implement
 * its custom behaviour.
 * 
 * @author Francesc Bautista
 *
 */
public abstract class AbstractMessage implements Message{

	protected int playerId;
	
	protected int nextTurn;
	
	protected int code = getCode();
	
	public AbstractMessage(){
		
	}
	

	
	@Override
	public String toJson(){

		Gson gson = new Gson();
		
		return gson.toJson(this);
	}

	
	
	public int getPlayerId(){
		return playerId;
	}



	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	
	public int getNextTurn(){
		return nextTurn;
	}
	
	public void setNextTurn(int next){
		this.nextTurn = next;
	}
	
	
}
