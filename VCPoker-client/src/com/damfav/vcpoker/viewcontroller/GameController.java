/* GameController.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.damfav.vcpoker.client.Client;
import com.damfav.vcpoker.message.ChangePlayerViewMessage;
import com.damfav.vcpoker.message.GameEndMessage;
import com.damfav.vcpoker.message.GameStartMessage;
import com.damfav.vcpoker.message.HandshakeMessage;
import com.damfav.vcpoker.message.Message;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.message.NewPeerMessage;
import com.damfav.vcpoker.message.NextTurnMessage;
import com.damfav.vcpoker.message.RoundEndMessage;
import com.damfav.vcpoker.model.Card;
import com.damfav.vcpoker.model.PublicProfile;
import com.damfav.vcpoker_client.R;

/**
 * The controller for the GameActivity, controlls all the flow of the game in
 * the client side
 */
public class GameController implements Observer, OnClickListener,
		DialogInterface.OnClickListener {

	/**
	 * The client instance
	 */
	private Client mClient;

	/**
	 * Callback to do when somebody wins
	 */
	@SuppressWarnings("unused")
	private Runnable mOnWinCallback;

	/**
	 * Own player's seat.
	 */
	private int ownPlayerSeat;

	/**
	 * Number of clients.
	 */
	private int nClients = 1;

	/**
	 * The game activity view instance
	 */
	private GameActivity mGameActivity;

	/**
	 * Biggest bet this round.
	 */
	private double lastBet;

	/**
	 * Whether the game is started.
	 */
	private boolean isStarted;

	/**
	 * Async task to show the progress of the time bar
	 */
	private ShowProgressTask spt;
	/**
	 * A TAG with the current class name to show in the log
	 */
	private static final String TAG = "GameController";

	/**
	 * Main Contructor of the GameController
	 * 
	 * @param gameActivity
	 *            The gameActivity where the game is running
	 * @param profileId
	 *            The player profile ID
	 * @param startGame
	 *            boolean to know if the user pressed Play now or Start game
	 */
	public GameController(GameActivity gameActivity, int profileId,
			final boolean startGame) {
		// set the parameter gameActivity to the field gameActivity
		this.mGameActivity = gameActivity;

		// get the instance of the client
		this.mClient = Client.getInstance();

		// add this class like a observer in the client
		this.mClient.addObserver(this);

		// create the show time progressbar
		this.spt = new ShowProgressTask();

		// create the callback when somebody wins the game
		this.mOnWinCallback = new Runnable() {

			@Override
			public void run() {
				// hide all the cards in the table
				mGameActivity.hideBoardCards();

			}

		};

		// start a new thread to run the game
		new Thread() {
			public void run() {

				if (!startGame) {
					// if the user selected play now just join a game
					mClient.joinGame();
				} else {
					// otherwise start a new game
					mClient.startGame();
				}
			}
		}.start();

	}

	@Override
	public void onClick(View v) {

		// create a new message
		Message msg = null;

		// Dispatch buttons
		switch (v.getId()) {
		case R.id.activity_game_btn_call:
			// when the call buttons is pressed create a call message
			msg = MessageFactory.createCallMessage();
			break;
		case R.id.activity_game_btn_allin:
			// When the all in button is pressed create the All in message with
			// all the current player money
			msg = MessageFactory.createAllInMessage();

			break;
		case R.id.activity_game_btn_check:
			// when the check button is pressed then create a new checkmessage
			msg = MessageFactory.createCheckMessage();

			break;
		case R.id.activity_game_btn_fold:
			Log.d(TAG, "Fold pressed!");
			// When the fold button is pressed then create a fold message
			msg = MessageFactory.createFoldMessage();

			break;
		case R.id.activity_game_btn_raise:
			// When the raise button is pressed show the raise dialog
			this.mGameActivity.showRaiseDialog(lastBet);
			break;
		default:
			break;
		}
		// call the method after action with the created message
		sendMsgToServer(msg);

	}

	/**
	 * This method send a message to the server to do a broadcast, disable all
	 * the button, and change turn
	 * 
	 * @param msg
	 *            message to send to server
	 */
	public void sendMsgToServer(Message msg) {
		// validate the message is different then null
		if (msg != null) {
			// send the message to client to send to the server
			mClient.sendMsg(msg, null);

		}
	}

	/**
	 * Receives the messages/actions from the players through the peer
	 * connection and executes them.
	 * 
	 */
	@Override
	public void update(Observable observableObject, final Object arg1) {
		mGameActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// get the changed message throw the parameter
				final Message message = (Message) arg1;
				if (message == null) {
					mGameActivity.onBackPressed();
					return;
				}
				int[] msgsThatRestartTimer = new int[] {
						MessageFactory.MSG_CALL, MessageFactory.MSG_ALLIN,
						MessageFactory.MSG_FOLD, MessageFactory.MSG_RAISE,
						MessageFactory.MSG_GAMESTART, MessageFactory.MSG_CHECK };

				for (int i = 0; i < msgsThatRestartTimer.length; i++) {
					if (msgsThatRestartTimer[i] == message.getCode()) {
						// restartTimer();
						break;
					}
				}

				Log.d(TAG, message.toJson());

				// get the player ID from the message
				int playerId = message.getPlayerId();
				Log.d(TAG, "PLAYERID: " + playerId);

				Log.d(TAG, message.getNextTurn() + "");
				mGameActivity.enableButtons(isStarted
						&& ownPlayerSeat == message.getNextTurn());

				// call the method onMessageRecieved to do actions depending in
				// the received codes
				onMessageRecieved(message, playerId);

			}
		});

	}

	/**
	 * This method is callen when the used is going to disconnect from the game
	 */
	public void disconnect() {

		Runnable callback = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// Disconnect the client from the server
				Log.d(TAG, "executing callback");
				mClient.onServerDisconnect();

				// Set the instance of the client to null
				Client.setInstance(null);
			}

		};

		// create a disconnect message and send it to the client to warn all the
		// connected player to disconect im from the game
		mClient.sendMsg(MessageFactory.createDisconnectMessage(), callback);

	}

	/**
	 * This method is called when the update method receive a message
	 * 
	 * @param message
	 *            the message received by the update method
	 * @param p
	 *            Player who has deliver the message
	 */
	private void onMessageRecieved(Message message, int pid) {

		switch (message.getCode()) {
		case MessageFactory.MSG_HANDSHAKE:
			HandshakeMessage msg = ((HandshakeMessage) message);
			this.ownPlayerSeat = msg.getOwnPlayerSeat();
			mGameActivity.addPlayer(this.ownPlayerSeat, "Player "
					+ this.ownPlayerSeat, msg.getPlayerCurrentMoney());
			mGameActivity.setUserCurrentMoney(msg.getPlayerCurrentMoney());
			mGameActivity.setOwnPlayerLayout(ownPlayerSeat);
			mGameActivity.setCurrentTableMoney(msg.getMoneyInTheTable());
			mGameActivity.showCards(msg.getCurrentCardsInTheTable());

			break;
		case MessageFactory.MSG_CHAT:
			// TODO pending to make a Chat

			break;

		case MessageFactory.MSG_CALL:
		case MessageFactory.MSG_RAISE:
		case MessageFactory.MSG_ALLIN:
			ChangePlayerViewMessage msgChangeView = (ChangePlayerViewMessage) message;

			mGameActivity.setPlayerMoney(pid,
					msgChangeView.getMoneyToSetInUserBid(),
					msgChangeView.getMoneyToSetInTheUserCurrentMoney());
			switch (message.getCode()) {
			case MessageFactory.MSG_CALL:
				mGameActivity.playSound(Constants.BET_RAISE_SOUND_ID);
				break;
			case MessageFactory.MSG_RAISE:
				mGameActivity.playSound(Constants.BET_RAISE_SOUND_ID);
				break;
			case MessageFactory.MSG_ALLIN:
				mGameActivity.playSound(Constants.ALL_IN_SOUND_ID);
				break;
			default:
				break;
			}

			break;

		case MessageFactory.MSG_FOLD:

			// Play the Fold sound in the game
			mGameActivity.playSound(Constants.FOLD_SOUND_ID);
			break;

		case MessageFactory.MSG_CHECK:

			// Play the Check sound in the game
			mGameActivity.playSound(Constants.CHECK_SOUND_ID);
			break;

		case MessageFactory.MSG_NEW:
			// Message received when a new player is connected

			// get the NewPeerMessage from the message
			NewPeerMessage nPM = (NewPeerMessage) message;

			// call the method onNewPeer to get all the information form the
			// message
			onNewPeer(nPM);
			nClients++;
			break;

		case MessageFactory.MSG_ROUNDEND:
			// Message received to synchronize all the cards for all the users

			// cast the message to CardMessage
			RoundEndMessage roundEndMsg = (RoundEndMessage) message;
			mGameActivity.setCurrentTableMoney(roundEndMsg
					.getMoneyToPutInTheTable());
			Log.e(TAG, "tablemon: " + roundEndMsg.getMoneyToPutInTheTable());
			List<Card> newCards = roundEndMsg.getNewCards();
			int boardCardState = roundEndMsg.getBoardCardsState();
			if (boardCardState == RoundEndMessage.FLOP) {
				mGameActivity.showCards(newCards);
			} else {
				mGameActivity.showACard(newCards.get(0),
						boardCardState == RoundEndMessage.TURN);
			}
			mGameActivity.resetPlayersBidMoney();
			break;
		case MessageFactory.MSG_DISCONNECT:
			Log.d(TAG, "" + pid);
			mGameActivity.removePlayer(pid);
			nClients--;
			if (nClients == 1) {
				mGameActivity.onBackPressed();
			}
			break;
		case MessageFactory.MSG_GAMESTART:
			isStarted = true;
			GameStartMessage gameStart = (GameStartMessage) message;
			List<Card> ownCards = gameStart.getOwnCards();
			mGameActivity.dealCards();
			mGameActivity.showOwnCards(ownCards);

			break;
		case MessageFactory.MSG_GAMEEND:
			isStarted = false;
			mGameActivity.enableButtons(false);
			GameEndMessage gameEnd = (GameEndMessage) message;
			Map<Integer, Double> winnerPot = gameEnd.getWinnerPot();
			List<Card> cards = gameEnd.getCardsLeftToShow();

			mGameActivity.setCurrentTableMoney(gameEnd
					.getMoneyToPutInTheTable());

			if (gameEnd.getCardsLeftToShow() != null) {
				System.err
						.println("cards received in gameend: " + cards.size());
				mGameActivity.showCardsLeft(cards);

			}

			for (Map.Entry<Integer, Double> winn : winnerPot.entrySet()) {
				GamePlayerView pview = mGameActivity.getPlayerById(winn
						.getKey());
				mGameActivity.setPlayerMoney(
						winn.getKey(),
						0,
						winn.getValue()
								+ Double.parseDouble(pview
										.getTextViewUserMoney().getText()
										.toString()));
				Log.e(TAG, " " + winn.getKey() + " " + winn.getValue());
			}

			mGameActivity.doWinAnimation(winnerPot);
			mGameActivity.getTableCurrentMoney().setText("0.0");
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					mGameActivity.hideBoardCards();
				}
			}, 3000);

			mGameActivity.resetPlayersBidMoney();
			break;
		case MessageFactory.MSG_NEXTTURN:
			Log.e(TAG, "nextturnmessage");
			NextTurnMessage nextTurnMsg = (NextTurnMessage) message;
			boolean[] possBtns = nextTurnMsg.getPossibleButtons();

			lastBet = nextTurnMsg.getLastBet();
			Button[] buttons = mGameActivity.getButtons();
			for (int i = 0; i < buttons.length; i++) {
				buttons[i]
						.setVisibility(possBtns[i] ? View.VISIBLE : View.GONE);
			}
			break;
		case MessageFactory.MSG_ADDCHIPS:
			mGameActivity.setPlayerMoney(message.getPlayerId(), 0, 200);
			break;
		default:
			break;
		}

	}

	/**
	 * Creates a player from the NewPeerMessage passed and adds it to the Game
	 * on seat = player.getId. If the Game is not started, game.initGame is
	 * called.
	 * 
	 * @param nPM
	 */
	private void onNewPeer(NewPeerMessage nPM) {

		if (mGameActivity.getProgressDialog().isShowing()) {
			mGameActivity.getProgressDialog().dismiss();
		}

		mGameActivity.addPlayer(nPM.getPlayerId(), nPM.getUsername(),
				nPM.getCurrentMoney());

		// restartTimer();

	}

	/**
	 * This method return a PublicProfile seated in the X table position
	 * 
	 * @param seatPos
	 *            The position of the player in the table
	 * @return PublicProfile of the player seated in that position
	 */
	public PublicProfile getPlayerProfile(int seatPos) {
		// TODO pending to implement
		return null;
	}

	/**
	 * This method restart the progressbar timer in the {@link GameActivity}
	 */
	public void restartTimer() {
		Log.d(TAG, "retarting timer");

		if (this.spt.getStatus() != AsyncTask.Status.FINISHED) {
			Log.d(TAG, "running...");
			// if the async task status is not finished then call the cancel
			// method of the task to stop the asynctask
			this.spt.cancel(true);
		}
		// create a new asynctask for the next turn
		this.spt = new ShowProgressTask();

		// execute the new task to start the countdown
		this.spt.execute();

	}

	/**
	 * This onclick method is used by the raise button
	 */
	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		// create a new message
		Message msg = null;

		// if the message is positive
		if (arg1 == DialogInterface.BUTTON_POSITIVE) {
			// get the count from the seekbar in the dialog
			int quant = mGameActivity.getSeekBar().getProgress();

			// create a new raise message with the amount selected by the user
			msg = MessageFactory.createRaiseMessage(quant);

		}

		// dismiss the dialog to keep player play the game
		this.mGameActivity.getmRaiseDialog().dismiss();

		// call the afterAction method to send message and disable buttons
		sendMsgToServer(msg);
	}

	/**
	 * This {@link AsyncTask} is created to update the time bar in the game
	 * activity
	 * 
	 */
	private class ShowProgressTask extends AsyncTask<Integer, Integer, Integer> {

		/**
		 * This method start starts a countdown from 10 to 1 with 1 second grab
		 * in each iteration
		 */
		@Override
		protected synchronized Integer doInBackground(Integer... totaltime) {
			Log.d(TAG, "doInBackground");
			int prog = 0;
			prog = 10;
			while (prog > 0) {
				if (this.isCancelled())
					break;
				publishProgress(prog);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				prog--;
				Log.i("ShowProgressTask", "current prog: " + prog);

			}
			return prog;
		}

		/**
		 * This method update the progressBar in the {@link GameActivity}
		 */
		@Override
		protected void onProgressUpdate(Integer... progress) {
			mGameActivity.setProgressBarTime(progress[0]);

		}

		/**
		 * This method is called when the 10 seconds are pass, then the task
		 * have to call the fold action
		 */
		@Override
		protected void onPostExecute(Integer result) {
			Log.d(TAG, "onPostExecute");

		}
	}

	// ------------ GETTERS AND SETTERS ------------

	public int getOwnPlayerSeat() {
		return ownPlayerSeat;
	}

	public void setOwnPlayerSeat(int ownPlayerSeat) {
		this.ownPlayerSeat = ownPlayerSeat;
	}

}
