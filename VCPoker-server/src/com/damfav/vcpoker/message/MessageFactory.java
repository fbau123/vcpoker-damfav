/* MessageFactory.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;

import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.player.Player;
import com.damfav.vcpoker.player.PlayerAction;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * An implementation of the Factory pattern. This class lets you create a
 * Message from a json String. Also it contains methods that give you a
 * reference to a Message that holds an instance to any of its subclasses.
 * 
 * @author Francesc Bautista
 * 
 */
public class MessageFactory {

	/**
	 * The Message codes.
	 */
	public static final int MSG_CHAT = 1;
	public static final int MSG_RAISE = 2;
	public static final int MSG_FOLD = 3;
	public static final int MSG_CHECK = 4;
	public static final int MSG_CALL = 5;
	public static final int MSG_ALLIN = 6;
	public static final int MSG_NEW = 7;
	public static final int MSG_DISCONNECT = 8;
	public static final int MSG_GET_STATE = 9;
	public static final int MSG_HANDSHAKE = 12;
	public static final int MSG_CARDS = 13;
	public static final int MSG_GAMEEND = 14;
	public static final int MSG_ROUNDEND = 15;
	public static final int MSG_GAMESTART = 16;
	public static final int MSG_CHANGEVIEW = 17;
	public static final int MSG_NEXTTURN = 18;
	public static final int MSG_ADDCHIPS = 19;

	private static JsonParser mParser = new JsonParser();

	/**
	 * Given a Message encoded in json format returns the Message instance that
	 * represents it.
	 * 
	 * @param json
	 *            A String in json format created from Message.toJson()
	 * @return Message instance.
	 */
	public static Message create(String json) {
		// A reference to the Message instance that will be returned.
		Message msg = null;
		Gson gson = new Gson();

		// Get the Message.getCode()
		JsonElement obj = mParser.parse(json);
		final int code = obj.getAsJsonObject().get("code").getAsInt();

		// Create the corresponding instance.
		switch (code) {
		case MSG_CHAT:
			msg = gson.fromJson(json, ChatMessage.class);
			break;
		case MSG_RAISE:
			msg = gson.fromJson(json, RaiseMessage.class);
			break;
		case MSG_FOLD:
			msg = gson.fromJson(json, FoldMessage.class);
			break;
		case MSG_CHECK:
			msg = gson.fromJson(json, CheckMessage.class);
			break;
		case MSG_CALL:
			msg = gson.fromJson(json, CallMessage.class);
			break;
		case MSG_ALLIN:
			msg = gson.fromJson(json, AllInMessage.class);
			break;
		case MSG_NEW:
			msg = gson.fromJson(json, NewPeerMessage.class);
			break;
		case MSG_DISCONNECT:
			msg = gson.fromJson(json, DisconnectMessage.class);
			break;
		case MSG_CARDS:
			msg = gson.fromJson(json, CardsMessage.class);
			break;
		case MSG_NEXTTURN:
			msg = gson.fromJson(json, NextTurnMessage.class);
			break;
		default:
			break;
		}

		return msg;

	}

	public static Message createRaiseMessage(double moneyToPutInTheTable,
			double moneyToSetInTheUserCurrentMoney, double moneyToSetInUserBid) {
		return new RaiseMessage(moneyToPutInTheTable,
				moneyToSetInTheUserCurrentMoney, moneyToSetInUserBid);
	}

	public static Message createChatMessage(String text) {
		return new ChatMessage(text);
	}

	public static Message createFoldMessage() {
		return new FoldMessage();
	}

	public static Message createCallMessage(double moneyToPutInTheTable,
			double moneyToSetInTheUserCurrentMoney, double moneyToSetInUserBid) {
		return new CallMessage(moneyToPutInTheTable,
				moneyToSetInTheUserCurrentMoney, moneyToSetInUserBid);
	}

	public static Message createCheckMessage() {
		return new CheckMessage();
	}

	public static Message createAllInMessage(double moneyToPutInTheTable,
			double moneyToSetInTheUserCurrentMoney, double moneyToSetInUserBid) {
		return new AllInMessage(moneyToPutInTheTable,
				moneyToSetInTheUserCurrentMoney, moneyToSetInUserBid);
	}

	public static Message createDisconnectMessage() {
		return new DisconnectMessage();
	}

	public static Message createNewPeerMessage(Player p) {

		String username = p.getProfile().getName();
		double bidMoney = p.getCurrentBidMoney();

		double currentMoney = p.getPlayerCurrentMoney();
		int seat = p.getId();

		PlayerAction roundAction = p.getRoundAction();
		List<Card> bestHand = p.getProfile().getBestHand();

		Message msg = new NewPeerMessage(username, bidMoney, currentMoney,
				bestHand, roundAction, seat);

		msg.setPlayerId(seat);

		return msg;

	}

	public static Message createCardsMessage(List<Card> cards) {
		return new CardsMessage(cards);
	}

}
