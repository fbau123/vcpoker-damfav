package com.damfav.vpoker.models.domain;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This class represents a record in the training set used to 
 * analyze the behaviour of poker players.
 * @author francescbautista
 *
 */
@Document(collection="poker_events")
public class PokerEvent{
	@Id
	private ObjectId id;
	
	private List<String> input;
	private List<String> output;
	
	private int count;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public List<String> getInput() {
		return input;
	}
	public void setInput(List<String> input) {
		this.input = input;
	}
	public List<String> getOutput() {
		return output;
	}
	public void setOutput(List<String> output) {
		this.output = output;
	}
	
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public void incCount(){
		this.count++;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		result = prime * result + ((output == null) ? 0 : output.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PokerEvent other = (PokerEvent) obj;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.containsAll(other.input))
			return false;
		if (output == null) {
			if (other.output != null)
				return false;
		} else if (!output.containsAll(other.output))
			return false;
		return true;
	}
	
	
	
	
	

}
