package com.damfav.vpoker.models.constraints;

import java.util.Iterator;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.damfav.vpoker.models.domain.User;
import com.damfav.vpoker.models.repositories.UserRepository;



public class NotEmailExistsConstraintValidator implements ConstraintValidator<NotEmailExists, String> {

	
	private UserRepository userRep = new UserRepository();

	@Override
	public boolean isValid(String target, ConstraintValidatorContext context) {

		try {
			List<User> users = userRep.findAll();
			for(User u: users){
				if ( u.getEmail().equals(target) ){
					return false;
				}
			}
		} catch (Exception e) {
			
		}
		return true;
	}


	@Override
	public void initialize(NotEmailExists arg0) {
		// TODO Auto-generated method stub
		
	}


}
