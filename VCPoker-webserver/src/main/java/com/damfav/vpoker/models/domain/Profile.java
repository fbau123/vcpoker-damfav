package com.damfav.vpoker.models.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="profiles")
public class Profile extends AbstractDocument{

	@Id
	private ObjectId id;
	
	private String user_email;
	
	private String username;
	
	private String bestHand;
	
	private String totalMoney;

	private String avatarUrl;
	
	private double biggestWin;
	
	private int totalWins;
	
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBestHand() {
		return bestHand;
	}

	public void setBestHand(String bestHand) {
		this.bestHand = bestHand;
	}

	public String getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(String totalMoney) {
		this.totalMoney = totalMoney;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public double getBiggestWin() {
		return biggestWin;
	}

	public void setBiggestWin(double biggestWin) {
		this.biggestWin = biggestWin;
	}

	public int getTotalWins() {
		return totalWins;
	}

	public void setTotalWins(int totalWins) {
		this.totalWins = totalWins;
	}
	
	 
	
}
