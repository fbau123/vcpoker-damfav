 /* Bot.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.model.ai;

import java.util.List;

import com.damfav.vcpoker.game.Game;
import com.damfav.vcpoker.hand.Hand;
import com.damfav.vcpoker.player.Player;
import com.damfav.vcpoker.player.PlayerStatus;
import com.damfav.vcpoker.player.Profile;


/**
 * A class that models a Poker Bot in a game.
 * It is an extension of Player as this will try to fake to be
 * a real Player.
 * 
 * Has a modifable AI that will be responsible of the actions
 * that this bot takes.
 * 
 * @author francescbautista
 *
 */
public class Bot extends Player{

	/**
	 * The Game instance where this bot is playing. This will be used
	 * to get the information of the game and pass the info to its ai.
	 */
	private Game mGame;

	
	/**
	 * The ai that runs the bot.
	 */
	private AI mAi;
	
	public Bot(List<PlayerStatus> currentStatus, double currentBidMoney,
			double playerCurrentMoney, Hand hand, Profile profile) {
		super(currentStatus, currentBidMoney, playerCurrentMoney, hand, profile);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * =======================
	 * GETTERS & SETTERS
	 * =======================
	 */
	
	public Game getGame(){
		
		return mGame;
	}
	
	public void setGame(Game g){
		this.mGame = g;
	}

	public AI getAi() {
		return mAi;
	}

	public void setAi(AI ai) {
		this.mAi = ai;
	}
	
	
	
}
