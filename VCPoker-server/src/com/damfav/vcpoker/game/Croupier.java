/* Croupier.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.player.Player;
import com.damfav.vcpoker.player.PlayerAction;
import com.damfav.vcpoker.player.PlayerStatus;

/**
 * Class that models a Croupier in a Poker Game. This classes handles the card
 * dealing and player status.
 */
public class Croupier {

	/**
	 * Game where this crouper is working
	 */
	private Game game;
	
	/**
	 * Cards stack
	 */
	private List<Card> stack;

	/**
	 * The index of the next card that will be shown. This is done to avoid
	 * popping cards when a new card is shown on the table.
	 */
	private int stackIndex;

	/**
	 * Dealer's position at the table
	 */
	private int dealerPosition;

	public Croupier(Game game) {
		this.game = game;
		this.stack = new ArrayList<Card>();
		this.dealerPosition = 0;
		setStack();
	}

	/**
	 * Generates a brand new stack with all 52 cards. Kind is represented by an
	 * integer as follows: 1 (clubs) 2 (diamonds) 3 (hearts) 4 (spades) Rank is
	 * represented as an integer, being the ace(A) 1 and the king(K) 13.
	 */
	public void setStack() {
		this.stack.clear();

		for (int kind = 1; kind < 5; kind++) {
			for (int rank = 1; rank < 14; rank++) {
				this.stack.add(new Card(rank, kind));
			}
		}

	}

	/**
	 * Deals two cards to every active player at the table in two rounds.
	 * 
	 */
	@SuppressWarnings("unused")
	public void dealCards() {
		Player[] players = this.game.getPlayers();
		if (players[this.dealerPosition] == null) {
			this.dealerPosition = getNextActivePlayer(this.dealerPosition)
					.getId();
		}

		// Change status
		// Change dealer's status
		List<Player> activePlayers = game.getActivePlayers();
		List<PlayerStatus> dStat = players[this.dealerPosition]
				.getCurrentStatus();

		if (activePlayers.size() == 2) {
			dStat.add(PlayerStatus.DEALER);
			dStat.add(PlayerStatus.BIGBLIND);
			Player sb = getActivePlayersFromDealer().get(0);
			sb.getCurrentStatus().removeAll(sb.getCurrentStatus());
			sb.getCurrentStatus().add(PlayerStatus.SMALLBLIND);
		} else {
			Player bb = getActivePlayersFromDealer().get(1);
			dStat.add(PlayerStatus.DEALER);
		}

		List<Player> apfd = game.getActivePlayers();

		for (int i = 0; i < 2; i++) {

			for (int j = 0; j < apfd.size(); j++) {
				Card c = getOneCard();
				System.out.println(c);
				apfd.get(j).recalculateHand(c);
			}
		}

		this.dealerPosition++;
	}

	/**
	 * Gets next player allowed to take actions.
	 * 
	 * @param pos, position from which to search
	 * @return Player
	 */
	public Player getNextActivePlayer(int pos) {
		Player[] players = this.game.getPlayers();

		while (players[pos] == null
				|| players[pos].getRoundAction() == PlayerAction.FOLD) {
			if (pos < 8) {
				pos++;
			} else {
				pos = 0;
			}
		}

		return players[pos];
	}

	/**
	 * Makes a list of the active players ordered from the dealer's position.
	 * 
	 * @return List of players
	 */
	public List<Player> getActivePlayersFromDealer() {
		Player[] players = this.game.getPlayers();
		List<Player> apfd = new ArrayList<>();

		// To store just the players at the table
		int pos = this.dealerPosition < 8 ? this.dealerPosition + 1 : 0;
		for (int i = pos; i < players.length; i++) {
			// Avoid empty seats
			if (players[i] != null) {
				apfd.add(players[i]);
			}
		}

		if (this.dealerPosition != 0) {
			for (int i = 0; i < this.dealerPosition + 1; i++) {
				if (players[i] != null) {
					apfd.add(players[i]);
				}
			}
		}

		return apfd;

	}

	/**
	 * Sets the players' hand empty before a new game.
	 * 
	 */
	public void collectCards() {
		Player[] players = this.game.getPlayers();

		for (int i = 0; i < players.length; i++) {

			if (players[i] != null) {
				players[i].setHand(null);
			}

		}
	}

	/**
	 * Takes three random cards from the stack (removing them from it) as the
	 * flop.
	 * 
	 * @return three random cards from the stack (removing them from it) as the
	 *         flop
	 */
	public ArrayList<Card> flop() {

		ArrayList<Card> flop = new ArrayList<Card>();

		for (int i = 3; i > 0; i--) {

			flop.add(getOneCard());

		}

		return flop;
	}

	/**
	 * Takes one random card from the stack, and removes the card from the
	 * stack. Used for turn and river rounds, and for dealing the cards to the
	 * players as well.
	 * 
	 * @return one random card from the stack
	 */
	public Card getOneCard() {
		return this.stack.get(stackIndex++);
	}

	/**
	 * Gets all the money bet this round so far as a Currency object.
	 * 
	 * @return all the money bet this round so far as a Currency object.
	 */
	public double getAllTheMoney() {

		Player[] players = game.getPlayers();

		// Create a Currency variable with same currency type as the game and
		// amount 0
		double totalMoneyAtStake = 0;

		for (int i = 0; i < 9; i++) {

			if (players[i] != null) {
				totalMoneyAtStake += players[i].getCurrentBidMoney();
			}

		}

		return totalMoneyAtStake;

	}

	/**
	 * Shuffles the stack before a new game starts.
	 */
	public void shuffle() {
		Collections.shuffle(this.stack);
		this.stackIndex = 0;
	}

	// ---------- GETTERS AND SETTERS ----------
	
	public int getDealerPosition() {
		return dealerPosition;
	}

	public void setDealerPosition(int dealerPosition) {
		this.dealerPosition = dealerPosition;
	}

	public List<Card> getStack() {
		return stack;
	}

	public void setStack(List<Card> stack) {
		this.stack = stack;
	}

	public int getStackIndex() {
		return stackIndex;
	}

	public void setStackIndex(int stackIndex) {
		this.stackIndex = stackIndex;
	}

}
