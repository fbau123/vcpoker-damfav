package com.damfav.vpoker.models.utils;

public class StatusCodes {
	public static final int ACTIVE = 0;
	public static final int DELETED = 1;
}
