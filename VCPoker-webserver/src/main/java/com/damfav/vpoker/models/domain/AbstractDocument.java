package com.damfav.vpoker.models.domain;

import java.lang.reflect.Field;
import java.util.Map;

import com.google.gson.Gson;

public class AbstractDocument {
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String toJson(){
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public void setAttributes(Map<String, String[]> map){
		Field[] fields = getClass().getDeclaredFields();
		
		for (Field f : fields){
			if (map.containsKey(f.getName())){
				f.setAccessible(true);

				String val = map.get(f.getName())[0];
				try {
					if (f.getType() == int.class){
						f.setInt(this, Integer.parseInt(val));
					}else if(f.getType() == double.class){
						f.setDouble(this, Double.parseDouble(val));
					}else{
						f.set(this, val);
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
}
