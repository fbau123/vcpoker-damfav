/* Constants.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

/**
 * This class defines all the constants in the game
 * 
 */
public class Constants {
	/**
	 * Constraint to compare if the login is like GUEST
	 */
	public static final String GUESTLOGIN = "GUEST";

	/**
	 * Constraint to compare if the login is with app login
	 */
	public static final String APPLOGIN = "APPLOGIN";

	/**
	 * Constraint to compare if the login is with Facebook
	 */
	public static final String FACEBOOKLOGIN = "FACEBOOKLOGIN";

	/**
	 * Constraint to compare if the login is with Google plus
	 */
	public static final String GOOGLELOGIN = "GOOGLELOGIN";

	/*
	 * SharedPreferences Constants
	 */
	/**
	 * Key with has saved the user login type
	 */
	public static final String LOGINPREFERENCEKEY = "loginType";

	/**
	 * File where is saved the user login information
	 */
	public static final String LOGINPREFERENCEFILE = "loginFile";

	/**
	 * Email with the user is currently conected
	 */
	public static final String LOGGEDEMAIL = "loggedEmail";

	/*
	 * Guest profile File
	 */
	/**
	 * File name where to save the guest information in the internal storage
	 */
	public static final String GUESTPROFILEFILE = "guestProfile";

	/**
	 * Key to know if is the first login of the user
	 */
	public static final String GUESTFIRSTLOGINKEY = "isFirstGuestLogin";

	/*
	 * GameActivity Constants
	 */
	/**
	 * Max users in the table
	 */
	public static final int MAXTABLEPLAYERS = 9;

	/**
	 * Id of the new username EditText
	 */
	public static final int NEWUSERNAMEEDITTEXT = 13;

	/**
	 * Key to get the boolean with if the game is already started
	 */
	public static final String START_GAMEKEY = "startGame";

	/**
	 * Key to get the user profile id
	 */
	public static final String USERPROFILEIDKEY = "userProfileID";

	/*
	 * SOUNDS ID
	 */
	public static final int BET_RAISE_SOUND_ID = 0;
	public static final int ALL_IN_SOUND_ID = 1;
	public static final int CHECK_SOUND_ID = 2;
	public static final int FOLD_SOUND_ID = 3;
	public static final int DEAL_COINS_SOUND_ID = 4;
	public static final int DEALING_SOUND_ID = 5;
	public static final int TURN_CARD_SOUND_ID = 6;

	/*
	 * Login & Registration Constrants
	 */
	/**
	 * Code to tell server to login
	 */
	public static final int LOGINTYPE = 15;

	/**
	 * Code to tell the server to register
	 */
	public static final int REGISTRATIONTYPE = 16;

	/**
	 * Code to define everything ok
	 */
	public static final int OK = 200;

	/**
	 * Code to define the email wrong format
	 */
	public static final int EMAILWRONGFORMAT = 213;
	/**
	 * Code to define the email wrong format
	 */
	public static final int WRONGPASSWORD = 223;

	/**
	 * Code to define an error
	 */
	public static final int ERROR = 257;

	/*
	 * Profile Columns name
	 */
	public static final String AVATARURL = "avatarURL";
	public static final String BESTHAND = "bestHand";
	public static final String BIGGESTWIN = "";
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String SEX = "sex";
	public static final String TOTALMONEY = "totalMoney";
	public static final String TOTALWINS = "totalWins";

}
