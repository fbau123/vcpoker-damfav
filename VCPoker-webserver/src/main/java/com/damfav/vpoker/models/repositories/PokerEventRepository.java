package com.damfav.vpoker.models.repositories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.damfav.vpoker.models.domain.PokerEvent;
import com.google.gson.Gson;

public class PokerEventRepository extends AbstractRepository<PokerEvent> {

	public PokerEventRepository() {
		super(PokerEvent.class);
		// TODO Auto-generated constructor stub
	}
	
	public String dataTableJsonFeed(){
		PokerEventRepository pkRepo = new PokerEventRepository();
		List<PokerEvent> pkEvents = pkRepo.findAll(); 
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sEcho", 1);
		map.put("iTotalRecords", pkEvents.size());
		map.put("iTotalDisplayRecords", 10);
		List<String[]> out = new ArrayList<String[]>();
		out.add(new String[]{"1c 2cc 3d x x", "bet"});
			
		map.put("aaData", out);
		
		String json = new Gson().toJson(map);
			
		return json;
	}
	
	public PokerEvent findByExample(PokerEvent ev){
		List<String> input =ev.getInput();
		List<String> output = ev.getOutput();
		
		Criteria c = new Criteria();
		c.and("input").all(input);
		c.and("output").all(output);
		
		return findOne(new Query(c));
	}
	

}
