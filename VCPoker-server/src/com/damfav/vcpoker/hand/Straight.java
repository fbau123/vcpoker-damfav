 /* Straight.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.Collections;
import java.util.List;

/**
 * A class that models the Straight hand.
 * 
 * @author francescbautista
 *
 */
public class Straight extends AbstractHand {

	public Straight(List<Card> cards) {
		super(cards);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected int concreteCompare(Hand other) {
		// TODO Auto-generated method stub
		Straight straightOther = (Straight) other;
		return findHighestCardOfStraight().getRank() - straightOther.findHighestCardOfStraight().getRank();
	}
	
	public Card findHighestCardOfStraight(){
		List<Card> lCards = getCards();
		Collections.sort(lCards, orderByRank);
		for (int i = 0; i < lCards.size() - 4; i++){
			
			if (isStraight(i)){
				return lCards.get(i);
			}
			
		}
		
		return null;
	}
	
	public boolean isStraight(int ini){
		for (int j = ini; j < ini + 4; j++){
			if (cards.get(j).getRank() != cards.get(j + 1).getRank() + 1){
				return false;
			}
		}
		return true;
	}
	
	
	public static boolean are(List<Card> cards){
		Straight h = new Straight(cards);
		return h.findHighestCardOfStraight() != null;
	}

	@Override
	public int getBaseValue() {
		// TODO Auto-generated method stub
		return 4;
	}

}
