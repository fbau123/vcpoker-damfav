package com.damfav.vpoker.models.repositories;

import static org.junit.Assert.*;

import java.util.List;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.DefaultBindingErrorProcessor;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.damfav.vpoker.models.constraints.NotEmailExistsConstraintValidator;
import com.damfav.vpoker.models.domain.User;
import com.damfav.vpoker.models.forms.SignUpForm;
import com.damfav.vpoker.models.repositories.UserRepository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
(
  {
   "file:src/main/webapp/WEB-INF/spring/root-context.xml",
   "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml",
  }
)
public class UserRepositoryTest {
	UserRepository userRep = new UserRepository();
	

	@Test
	public void testSave(){
		User u = new User();
		u.setEmail("test@test.com");
		u.setPassword("gggg");
		int sizeb4 = userRep.findAll().size();
		userRep.save(u);
		int sizeaft = userRep.findAll().size();
		assertTrue(sizeaft == sizeb4 + 1);
		
	}
	
	@Test
	public void testFind(){
		
		User u = userRep.findOne(query(where("email").is("test@test.com")) );
		
		assertEquals(u.getPassword(), "gggg");
	}
	
	@Test
	public void testExists(){
		
		User u = userRep.findOne(query(where("email").is("test@test.com")) );
		
		assertTrue(userRep.exists(u.getId()));
	}
	

}
