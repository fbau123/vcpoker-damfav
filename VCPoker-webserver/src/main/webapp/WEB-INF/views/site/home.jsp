<%@page import="com.damfav.vpoker.views.JspHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%
	JspHelper.begin();
%>
<%
	JspHelper.addCss("/home.css");
%>

<% JspHelper.addScript("/home.js"); %>
<t:generic>
<div class="slide" id="slide1">
		<div class="row">
			<div class="col-md-6">
				<img src="/resources/images/virtualmoney.png" alt="bitcoin" />
			</div>
			<div class="col-md-6">
				<p>Text</p>
			</div>
		</div>
</div>

<div class="slide" id="slide2">
	<h2>Slide2</h2>
</div>

<div class="slide" id="slide3">
	<h2>Slide3</h2>
</div>

<div class="slide" id="slide4">
	<h2>Slide4</h2>
</div>

</t:generic>
