package com.damfav.vpoker.views;

import java.util.ArrayList;
import java.util.List;

import com.damfav.vpoker.models.domain.PokerEvent;

public class DataTable {

	public static List<String[]> transformStatisticsJson(
			List<PokerEvent> pkEvents) {
		List<String[]> jsonReady = new ArrayList<String[]>();
		for (PokerEvent ev : pkEvents) {
			String input = "";

			for (String s : ev.getInput()) {
				String imgName = "";

				if (s.equals("x")) {
					imgName = "back.png";
				} else {
					String num = s.length() == 3 ? s.substring(0, 2) : s
							.substring(0, 1);
					switch (num) {
					case "1":
						num = "ace";
						break;
					case "11":
						num = "jack";
						break;
					case "12":
						num = "queen";
						break;
					case "13":
						num = "king";
						break;
					}

					String kind = s.substring(s.length() - 1);
					switch (kind) {
					case "c":
						kind = "clubs";
						break;
					case "h":
						kind = "hearts";
						break;
					case "s":
						kind = "spades";
						break;
					case "d":
						kind = "diamonds";
						break;

					}
					imgName = num + "_of_" + kind + ".png";
				}
				input += "<img class='card' src='/resources/images/cards/"
						+ imgName + "' alt='pk_card' />";
			}

			String output = "";
			for (String s : ev.getOutput()) {
				output += s + " ";
			}
			jsonReady.add(new String[] { input, output });
		}
		return jsonReady;
	}
}
