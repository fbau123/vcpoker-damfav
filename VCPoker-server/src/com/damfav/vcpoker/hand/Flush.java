 /* Flush.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.List;

/**
 * A class that models the Flush hand
 * 
 * @author francescbautista
 *
 */
public class Flush extends AbstractHand {

	public Flush(List<Card> cards) {
		super(cards);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected int concreteCompare(Hand other) {		
		Flush flushOther = (Flush) other;
		return findMaxKind().getRank() - flushOther.findMaxKind().getRank() ;
	}
	
	private Card findMaxKind(){
		sort();
		return cards.get(0).getKind() == cards.get(1).getKind() ? 
				cards.get(0) : cards.get(1);
			
	}
	
	public static boolean are(List<Card> cards){
		boolean found = false;
		for (int i = 0; i < cards.size() - 4 && !found; i++){
			int count = 1;
			
			for (int j = i + 1; j < cards.size(); j++){
				if (cards.get(i).getKind() == cards.get(j).getKind()){
					count++;
				}
			}
			if (count >= 4){
				found = true;
			}
			
			
		}
		
		return found;
	}

	@Override
	public int getBaseValue() {
		// TODO Auto-generated method stub
		return 5;
	}

}
