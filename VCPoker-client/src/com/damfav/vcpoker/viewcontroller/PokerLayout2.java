/* PokerLayout2.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.damfav.vcpoker_client.R;

/**
 * Poker layout used to create a gameActivity programatically.
 */
public class PokerLayout2 extends RelativeLayout {

	GamePlayerView[] players = new GamePlayerView[9];
	int playerWPerc = 15;
	int playerHPerc = 18;
	int[] rowsX = new int[4];
	private ImageView pokerImg;
	/**
	 * X-distances from the edge of the screen to the position of the player for
	 * both ends of the row.
	 * 
	 * example: --x-------x-- -x---------x-
	 */
	private static int defaultRow1X = 10;
	private static int defaultRow2X = 5;
	private static int defaultRow3X = 5;
	private static int defaultRow4X = 10;

	public PokerLayout2(Context context, AttributeSet attrs) {
		super(context, attrs);
		rowsX[0] = attrs.getAttributeIntValue("custom", "row1X", defaultRow1X);
		rowsX[1] = attrs.getAttributeIntValue("custom", "row23X", defaultRow2X);
		rowsX[2] = attrs.getAttributeIntValue("custom", "row23X", defaultRow3X);
		rowsX[3] = attrs.getAttributeIntValue("custom", "row4X", defaultRow4X);

		pokerImg = new ImageView(context);
		RelativeLayout.LayoutParams imgParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		pokerImg.setLayoutParams(imgParams);
		pokerImg.setImageResource(R.drawable.hall);

		this.setBackgroundColor(Color.TRANSPARENT);
		this.addView(pokerImg);
		// this.addView(pot);
		// this.addView(cardLayout);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		View parent = (View) this.getParent();
		int rootW = parent.getWidth();
		int rootH = parent.getHeight();
		int playerWidth = (playerWPerc * rootW) / 100;
		int playerHeight = (playerHPerc * rootH) / 100;
		int ySeparation = (rootH - 4 * playerHeight) / 3;

		for (int i = 0; i < players.length - 3; i++) {

			if (players[i] != null) {
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) players[i]
						.getLayoutParams();
				params.width = playerWidth;
				params.height = playerHeight;
				if (i % 2 == 0) {
					params.leftMargin = (rowsX[i / 2] * rootW) / 100;
				} else {
					params.leftMargin = ((100 - rowsX[i / 2]) * rootW) / 100
							- playerWidth;
				}
				params.topMargin = (playerHeight + ySeparation) * (i / 2);
			}

		}

		for (int i = 0; i < 3; i++) {
			GamePlayerView but = players[6 + i];
			if (but != null) {
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) but
						.getLayoutParams();
				params.width = playerWidth;
				params.height = playerHeight;
				if (i == 0) {
					params.leftMargin = (rowsX[3] * rootW) / 100;
				} else if (i == 1) {
					params.addRule(RelativeLayout.CENTER_HORIZONTAL,
							RelativeLayout.TRUE);
				} else {
					params.leftMargin = ((100 - rowsX[3]) * rootW) / 100
							- playerWidth;
				}
				params.topMargin = (playerHeight + ySeparation) * 3;
			}
		}

		super.onLayout(changed, l, t, r, b);
	}

	/**
	 * Add player at pos. Pos 1 is top left. Pos 2 is top right. . . . Pos 9
	 * bottom right.
	 * 
	 * @param pos
	 */
	public void addPlayer(int pos) {
		GamePlayerView b = new GamePlayerView(getContext());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				20, 20);
		b.setLayoutParams(params);

		this.addView(b);

		players[pos - 1] = b;
	}

	// ----------- GETTERS AND SETTERS -----------
	
	public GamePlayerView[] getPlayers() {
		return players == null ? null : (GamePlayerView[]) players.clone();
	}
	
	
}
