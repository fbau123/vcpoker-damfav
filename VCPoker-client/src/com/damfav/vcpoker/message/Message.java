/* Message.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

/**
 * The interface for all the Message that peers will use to communicate with
 * each other.
 * 
 */
public interface Message {

	/**
	 * Converts this message to json format to send through sockets. The
	 * MessageFactory contains a method to create a Message from json, that in
	 * combination with this one lets you create a json comunication between
	 * peers.
	 * 
	 * @return String The message converted to json.
	 */
	String toJson();

	/**
	 * The code that identifies the type of this message. The method getClass()
	 * would also work, but this makes it easier to send by json. The codes can
	 * be found on MessageFactory.
	 * 
	 * @return int the code of this Message.
	 */
	int getCode();

	/**
	 * The id of the player that sends the message.
	 * 
	 * @return int the playerId
	 */
	int getPlayerId();

	/**
	 * The Seat of the layer how is next
	 * 
	 * @return the next player seat number
	 */
	int getNextTurn();

	/**
	 * Setter for the next turn
	 * 
	 * @param who
	 *            will be next in the game
	 */
	void setNextTurn(int next);
}
