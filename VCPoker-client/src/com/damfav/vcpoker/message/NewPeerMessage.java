/* NewPeerMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;

import com.damfav.vcpoker.model.Card;

/**
 * The message that will be sent when a new connection to a peer is made.
 * 
 */
public class NewPeerMessage extends AbstractMessage {

	/**
	 * THe other player seat number
	 */
	private int seat;

	/**
	 * The other player username
	 */
	private String username;

	/**
	 * The player bid money
	 */
	private double bidMoney;

	/**
	 * The other player current money
	 */
	private double currentMoney;

	public NewPeerMessage() {
	}

	public NewPeerMessage(String username, double bidMoney,
			double currentMoney, List<Card> userCards, int seat) {
		super();
		this.username = username;
		this.bidMoney = bidMoney;
		this.currentMoney = currentMoney;
		this.seat = seat;
	}

	@Override
	public int getCode() {
		return MessageFactory.MSG_NEW;
	}

	// GETTERS & SETTERS
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getBidMoney() {
		return bidMoney;
	}

	public void setBidMoney(double bidMoney) {
		this.bidMoney = bidMoney;
	}

	public double getCurrentMoney() {
		return currentMoney;
	}

	public void setCurrentMoney(double currentMoney) {
		this.currentMoney = currentMoney;
	}

	public int getSeat() {
		return seat;
	}

	public void setSeat(int seat) {
		this.seat = seat;
	}

}
