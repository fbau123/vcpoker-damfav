 /* RoundEndMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;

import com.damfav.vcpoker.hand.Card;

public class RoundEndMessage extends AbstractMessage{

	public static final int FLOP = 0;
	public static final int TURN = 1;
	public static final int RIVER = 2;
	private double moneyToPutInTheTable;
	private List<Card> newCards;
	private int boardCardsState;
	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return MessageFactory.MSG_ROUNDEND;
	}
	public List<Card> getNewCards() {
		return newCards;
	}
	public void setNewCards(List<Card> newCards) {
		this.newCards = newCards;
	}
	public int getBoardCardsState() {
		return boardCardsState;
	}
	public void setBoardCardsState(int boardCardsState) {
		this.boardCardsState = boardCardsState;
	}
	public double getMoneyToPutInTheTable() {
		return moneyToPutInTheTable;
	}
	public void setMoneyToPutInTheTable(double moneyToPutInTheTable) {
		this.moneyToPutInTheTable = moneyToPutInTheTable;
	}
	
	
	
}
