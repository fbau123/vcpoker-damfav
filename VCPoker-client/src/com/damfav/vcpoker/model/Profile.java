/* Profile.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The private Profile of a user. Holds all the information of the publicProfile
 * and some more.
 */
public class Profile extends PublicProfile {

	/**
	 * The id of the user in the database
	 */
	private int id;

	/**
	 * All the money the player has.
	 */
	private double totalMoney;
	
	/**
	 * Converts a JSON object into a Profile.
	 * 
	 * @param obj, the JSON object to be converted
	 * @return a Profile object
	 */
	public static final Profile fromJson(JSONObject obj) {
		Profile p = new Profile();
		try {
			p.setBiggestWin((Double) obj.get("biggestWin"));
			p.setTotalWins((Integer) obj.get("totalWins"));
			p.setName((String) obj.get("username"));
			p.setTotalMoney(1000);
			p.setAvatarURL((String) obj.get("avatarUrl"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return p;

	}

	// ------------ GETTERS AND SETTERS -----------

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}
	

}
