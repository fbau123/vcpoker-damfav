package com.damfav.vpoker.models.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = SignUpFormConstraintValidator.class)
public @interface ValidSignUpForm {
	String message() default "Email exists";

	Class<?>[] groups() default {};

	Class<?>[] payload() default {};
}
