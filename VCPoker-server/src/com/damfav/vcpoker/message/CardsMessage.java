 /* CardsMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;

import com.damfav.vcpoker.hand.Card;

/**
 * A message that models the action of synchronizing the stack cards.
 * This is needed because without it every player would shuffle their
 * cards on a different way.
 * 
 * @author francescbautista
 *
 */
public class CardsMessage extends AbstractMessage{

	private List<Card> stack;
	

	
	public CardsMessage(List<Card> stack){
		this.stack = stack;
	}
	
	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return MessageFactory.MSG_CARDS;
	}



	public List<Card> getStack() {
		return stack;
	}

	public void setStack(List<Card> stack) {
		this.stack = stack;
	}
	
	

}
