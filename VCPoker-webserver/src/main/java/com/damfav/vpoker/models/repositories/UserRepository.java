package com.damfav.vpoker.models.repositories;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.damfav.vpoker.models.domain.User;
import com.damfav.vpoker.models.utils.MD5Encrypter;

public class UserRepository extends AbstractRepository<User> {

	public UserRepository() {
		super(User.class);
		// TODO Auto-generated constructor stub
	}
	
	public boolean correctLogin(User u){
		Criteria c = new Criteria();
		
		c.and("email").is(u.getEmail());
		c.and("password").is( MD5Encrypter.encryptPassword(u.getPassword()) );
		
		Query q = new Query(c);
		
		return exists(q);
	}
	
	public boolean exists(User u){
		Criteria c = new Criteria();
		c.and("email").is(u.getEmail());
		
		return exists(new Query(c));
	}
	
}