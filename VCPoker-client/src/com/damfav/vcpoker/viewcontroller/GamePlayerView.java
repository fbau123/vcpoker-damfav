/* GamePlayerView.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.damfav.vcpoker.model.PublicProfile;
import com.damfav.vcpoker_client.R;

/**
 * This class represent a game player view
 */
public class GamePlayerView extends RelativeLayout implements OnClickListener {
	/**
	 * The username textview
	 */
	private TextView textViewUserName;

	/**
	 * The user profile image
	 */
	private ImageView imageViewUserProfilePicture;

	/**
	 * The user money in the game
	 */
	private TextView textViewUserMoney;

	/**
	 * The user money in the table
	 */
	private TextView textViewUserMoneyInTheTable;

	/**
	 * The context where the game player will be
	 */
	private Context context;

	/**
	 * The user linnearlayout where are all the information of the player
	 */
	private RelativeLayout userLinearLayout;

	/**
	 * The own user position
	 */
	private int seatPos;

	/**
	 * The gameActivity where the player is shown
	 */
	private GameActivity mGameActivity;

	/**
	 * This field is used to tag the debug message in the logcat
	 */
	private static final String TAG = "GamePlayerView";

	private boolean usedLayout;

	/**
	 * Default constructor of the {@link GamePlayerView} class
	 * 
	 * @param context
	 *            {@link Context} of the application
	 */
	public GamePlayerView(Context context) {
		super(context);
		// save the context in the field
		this.context = context;
		// init the game player view with a null attributes
		initGamePlayerView(context, null);

	}

	/**
	 * Main contructor of the {@link GamePlayerView} class
	 * 
	 * @param context
	 *            The context of the class
	 * @param attrs
	 *            The attributes of the layout
	 */
	public GamePlayerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// save the context in the field
		this.context = context;
		// init the game player view with received attributes
		initGamePlayerView(context, attrs);
		clear();
	}

	/**
	 * This method initialize the {@link GamePlayerView}
	 * 
	 * @param context
	 *            The context of the application
	 * @param attrs
	 *            The attributes of the layout
	 */
	private void initGamePlayerView(Context context, AttributeSet attrs) {
		// set the listener in the hwo view
		this.setOnClickListener(this);

		// inflate a new RelativeLayout
		userLinearLayout = (RelativeLayout) View.inflate(context,
				R.layout.userlayout, null);

		this.addView(this.userLinearLayout, new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		Log.e(TAG, "after inflate player");
		// get a typedArray with the costume attribute
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.GamePlayerView, 0, 0);

		// create a default orientation in the player view
		int bidOrientation = 0;
		try {
			// get the orientaton declared in the xml layout
			bidOrientation = ta.getInt(
					R.styleable.GamePlayerView_bidorientation, -1);
		} finally {
			ta.recycle();
		}
		Log.e(TAG, "after bidori " + bidOrientation);

		// get all the view and save him inthe fields
		this.textViewUserName = (TextView) this.userLinearLayout
				.findViewById(R.id.usernameTextview);
		this.textViewUserMoney = (TextView) this.userLinearLayout
				.findViewById(R.id.userMoneyTextview);
		this.imageViewUserProfilePicture = (ImageView) this.userLinearLayout
				.findViewById(R.id.userImageImageview);
		this.textViewUserMoneyInTheTable = (TextView) this.userLinearLayout
				.findViewById(R.id.bidtextview);

		/*
		 * Bid layout orientation 0 == Down || 1 == Left || 2 == Up || 3 ==Right
		 */
		// if the orientation is looking down or up enter in vertical
		// orientation
		if (bidOrientation == 0 || bidOrientation == 2) {

			Log.d(TAG, "changed to Vertical");
			// depending in the orientation add first the userlayout or
			// bidlayout
			RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) textViewUserMoneyInTheTable
					.getLayoutParams();

			textViewUserMoney.bringToFront();
			textViewUserMoney.invalidate();

			if (bidOrientation == 0) {
				params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
						RelativeLayout.TRUE);
			} else {
				params.addRule(RelativeLayout.ALIGN_PARENT_TOP,
						RelativeLayout.TRUE);
			}
			textViewUserMoneyInTheTable.setLayoutParams(params);

		}

		Log.e(TAG, "after add player");
	}

	public void clear() {
		this.textViewUserMoney.setText("");
		this.textViewUserMoneyInTheTable.setText("");
		this.textViewUserName.setText("");
	}

	@Override
	public void onClick(View v) {
		// create a new dialog to show the other use information
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// set the content of the dialog with the created xml layout
		dialog.setContentView(R.layout.activity_game_rl_userinfo);

		// get the GamePlayerView
		GamePlayerView gpv = (GamePlayerView) v;

		// get the COntroller of the view
		GameController c = ((GameActivity) this.context).getController();

		// get the public profile of the user seated in that position
		PublicProfile p = c.getPlayerProfile(gpv.getSeatPos());

		// if the user seat there are different then the null
		if (p != null) {

			// set all the information in the created dialog
			((TextView) dialog.findViewById(R.id.userInfo_tv_Username))
					.setText(p.getName());
			((TextView) dialog.findViewById(R.id.userInfo_tv_CurrentMoney))
					.setText(gpv.getTextViewUserMoney().getText());
			// ((TextView) dialog.findViewById(R.id.userInfo_tv_BiggestWin))
			// .setText("" + p.getBiggestWin().getAmount());
			// ((TextView) dialog.findViewById(R.id.userInfo_tv_totalsWin))
			// .setText(p.getTotalWins());
		}

		// show the dialog to the user
		dialog.show();

	}

	// ------------ GETTERS AND SETTERS ------------

	public GameActivity getmGameActivity() {
		return mGameActivity;
	}

	public void setmGameActivity(GameActivity mGameActivity) {
		this.mGameActivity = mGameActivity;
	}

	public int getSeatPos() {
		return seatPos;
	}

	public void setSeatPos(int seatPos) {
		this.seatPos = seatPos;
	}

	public TextView getTextViewUserName() {
		return textViewUserName;
	}

	public ImageView getImageViewUserProfilePicture() {
		return imageViewUserProfilePicture;
	}

	public TextView getTextViewUserMoney() {
		return textViewUserMoney;
	}

	public TextView getTextViewUserMoneyInTheTable() {
		return textViewUserMoneyInTheTable;
	}

	public void setUserName(String name) {
		this.textViewUserName.setText(name);
	}

	public void setImageViewUserProfilePicture(Drawable d) {
		imageViewUserProfilePicture.setImageDrawable(d);
	}

	public void setUserMoney(double currency) {
		this.textViewUserMoney.setText("" + currency);
	}

	public boolean isUsedLayout() {
		return usedLayout;
	}

	public void setUsedLayout(boolean usedLayout) {
		this.usedLayout = usedLayout;
	}

	/**
	 * This method set's the user bid money
	 * 
	 * @param money
	 *            The money to put in the bid money layout
	 */
	public void setUserBidMoney(double money) {
		textViewUserMoneyInTheTable.setText("" + money);
	}

}
