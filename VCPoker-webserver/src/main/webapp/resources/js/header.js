/**
 * 
 */

function MHeader(){
	this.loginLoaded = false;
	this.singUpLoaded = false;
};

MHeader.prototype = {	

		
	highLightCurrent : function () {
		var currentPath = location.pathname;
		$("#main-nav li").each(function(index) {
			if ($(this).find("a").attr("href") == currentPath) {
				$(this).addClass("active");
			}
		});
	},
	

	
	initEventListeners : function (){
		var that = this;
		$("body").on("click", ".logoutBtn",function() {
			that.logout();
		});
		
		$("body").on("click", ".loginBtnModal", function(){
			that.login();
		});
		
		$("body").on("click", ".signUpBtnModal", function(){
			that.signUp();
		});
		
		$("body").on("click", ".loginBtn", function(){
			that.showLoginModal();
		});
		
		$("body").on("click", ".signUpBtn", function(){
			that.showSignUpModal();
		});
	},
	
	showLoginModal : function(){
		if (!this.loginLoaded){
			$("#loginModal .modal-body").load("/user/getLoginForm");
			this.loginLoaded = true;
		}
		$("#loginModal").modal("show");
	},
	
	showSignUpModal : function(){
		if (!this.singUpLoaded){ 
			$("#signUpModal .modal-body").load("/user/getSignUpForm");
			this.singUpLoaded = true;
		}
		$("#signUpModal").modal("show");
	},
	
	
	logout : function () {
		var that = this;
		$.ajax({
			type : "POST",
			url : "/user/logout",
			success : function(data, txtStatus, xhr) {
				that.modifyLoginSection(data);
			}
		});
	},
	
	login : function () {
		this._signUpLogin("#loginModal", "/user/login", this.modifyLoginSection);
	},
	
	_signUpLogin : function(modalSelector, url, callbackSuccess){
		var modal = $(modalSelector);
		var form = modal.find("form");
		var data = $(form).serialize();
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			success : function(data, txtStatus, xhr) {
				if (StatusHeader.isOk(xhr.status)) {
					if (callbackSuccess){
						callbackSuccess(data);
					}
					modal.modal('hide');
				} else {
					modal.find(".modal-body").html(data);
				}
			}
		});
	},
	
	signUp : function () {
		this._signUpLogin("#signUpModal", "/user/signUp", null);
	},
	
	modifyLoginSection : function (data) {
		var headerLoginSectionSel = "#loginSectionMainNav";
		$(headerLoginSectionSel).html(data);
	}

};

var mHeader = new MHeader();

$(document).ready(function() {
	mHeader.highLightCurrent();
	mHeader.initEventListeners();
});
