 /* AI.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.model.ai;

/**
 * The base interface for all the clases that want to act as the
 * artificial intelligence of the poker bot.
 * 
 * @author francescbautista
 *
 */
public interface AI {

	
	/**
	 * Called everytime the bot has a turn. This will look at the game
	 * state and change the currently executing behavior. 
	 * This is the most important method and where the definition of an
	 * AI resides. 
	 */
	void run();
	
	/**
	 * This method retrieves the information of the current game state
	 * and stores it so
	 */
	void queryServer();
	
	/**
	 * Calculates the effective hand strength.
	 * @see EffectiveHandCalculator
	 * 
	 * @return The ehs
	 */
	double calculateEffectiveHandStrenght();
	
	/**
	 * Returns the bot that this AI is attached to.
	 * 
	 * @return Bot
	 */
	Bot getBot();
	
	
	/**
	 * Attaches this AI to a bot.
	 * @param b
	 */
	void setBot(Bot b);
}
