<%@page import="com.damfav.vpoker.views.JspHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%
	JspHelper.begin();
%>

<%
	JspHelper.addScript("/statistics.js");
	JspHelper.addCss("/statistics.css");
%>
<t:generic>
	<h1>Stats</h1>
	<div style="padding:10px">
		<table id="dataTable">
		</table>
	</div>
</t:generic>
