 /* AbstractHand.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Abstract base class for Hand objects. This class implements all the common operations
 * of the Hand interface.
 * Subclasses must implement the method concreteCompare.
 * 
 * @author francescbautista
 *
 */
public abstract class AbstractHand implements Hand {

	/**
	 * A comparator that orders a collection of cards descendantly.
	 */
	public static Comparator<Card> orderByRank = new Comparator<Card>() {

		@Override
		public int compare(Card lhs, Card rhs) {
			// TODO Auto-generated method stub
			return rhs.getRank() - lhs.getRank();
		}


	};
	
	
	/**
	 * The list of cards wrapped by this Hand.
	 */
	protected List<Card> cards;
	
	public AbstractHand(List<Card> cards){
		this.cards = cards;
	}
	
	@Override
	public int compareTo(Hand other) {
		if (other.getClass() != this.getClass()){
			return this.getBaseValue() - other.getBaseValue();
		}else{
			return concreteCompare(other);
		}
	}
	
	/**
	 * Compares two hands that have have the same type.
	 * This comparation is different for every type of Hand.
	 * 
	 * @param other
	 * @return negative, 0, positive if this hand is smaller equal or bigger than other.
	 */
	protected abstract int concreteCompare(Hand other);

	@Override
	public List<Card> getCards() {
		// TODO Auto-generated method stub
		return cards;
	}
	
	@Override
	public Card findHighestSequence(int sequenceSize){
		return findHighestSequence(sequenceSize, -1);
	}
	
	@Override
	public Card findHighestSequence(int sequenceSize, int diffOfRank){
		Collections.sort(cards, orderByRank);
		int lastEle = cards.size() - sequenceSize;
		
		for (int i = 0; i <= lastEle; i++){
			
			if (isSequence(i, sequenceSize)
					&& diffOfRank != cards.get(i).getRank()){
				
				return cards.get(i);
				
			}
			
		}
		
		return null;
	}
	
	/**
	 * Checked the cards have a sequence of sequenceSize starting at index i.
	 * @param i
	 * @param sequenceSize
	 * @return true if there is a sequence.
	 */
	private boolean isSequence(int i, int sequenceSize){
		for (int j = i; j < i + sequenceSize - 1; j++){
			
			if ( !cards.get(j).rankEquals(cards.get(j+1)) ){
				return false;
			}
			
		}
		return true;
	}
	
	@Override
	public Card findHighestCard(){
		sort();
		return cards.get(0);
	}
	
	public void sort(){
		Collections.sort(cards, orderByRank);
	}

	/**
	 * This method returns true if the list of cards passed represent a Hand of 
	 * this type.
	 * 
	 * Subclasses should hide this method
	 * 
	 * @param cards
	 * @return true if the cards are a hand of this type.
	 */
	public static boolean are(List<Card> cards){
		return true;}
}
