/* GameEndMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import java.util.List;
import java.util.Map;

import com.damfav.vcpoker.model.Card;

/**
 * Model message to transfer information between server-client when the Game end
 */
public class GameEndMessage extends AbstractMessage {

	/**
	 * All the winners with the winned money
	 */
	private Map<Integer, Double> winnerPot;

	/**
	 * Cards left to show in the table
	 */
	private List<Card> cardsLeftToShow;
	private double moneyToPutInTheTable;

	@Override
	public int getCode() {
		return MessageFactory.MSG_GAMEEND;
	}



	// GETTERS & SETTERS
	public List<Card> getCardsLeftToShow() {
		return cardsLeftToShow;
	}

	public void setCardsLeftToShow(List<Card> cardsLeftToShow) {
		this.cardsLeftToShow = cardsLeftToShow;
	}



	public Map<Integer, Double> getWinnerPot() {
		return winnerPot;
	}



	public void setWinnerPot(Map<Integer, Double> winnerPot) {
		this.winnerPot = winnerPot;
	}



	public double getMoneyToPutInTheTable() {
		return moneyToPutInTheTable;
	}



	public void setMoneyToPutInTheTable(double moneyToPutInTheTable) {
		this.moneyToPutInTheTable = moneyToPutInTheTable;
	}

}
