/* MessageFactory.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * An implementation of the Factory pattern. This class lets you create a
 * Message from a json String. Also it contains methods that give you a
 * reference to a Message that holds an instance to any of its subclasses.
 * 
 */
public class MessageFactory {

	/**
	 * The Message codes.
	 */
	public static final int MSG_CHAT = 1;
	public static final int MSG_RAISE = 2;
	public static final int MSG_FOLD = 3;
	public static final int MSG_CHECK = 4;
	public static final int MSG_CALL = 5;
	public static final int MSG_ALLIN = 6;
	public static final int MSG_NEW = 7;
	public static final int MSG_DISCONNECT = 8;
	public static final int MSG_GET_STATE = 9;
	public static final int MSG_HANDSHAKE = 12;
	public static final int MSG_GAMEEND = 14;
	public static final int MSG_ROUNDEND = 15;
	public static final int MSG_GAMESTART = 16;
	public static final int MSG_CHANGEVIEW = 17;
	public static final int MSG_NEXTTURN = 18;
	public static final int MSG_ADDCHIPS = 19;

	/**
	 * The parser to Convert String into a Java JsonElement
	 */
	private static JsonParser mParser = new JsonParser();

	/**
	 * Given a Message encoded in json format returns the Message instance that
	 * represents it.
	 * 
	 * @param json
	 *            A String in json format created from Message.toJson()
	 * @return Message instance.
	 */
	public static Message create(String json) {
		// A reference to the Message instance that will be returned.
		Message msg = null;
		Gson gson = new Gson();

		if (json == null) {
			return null;
		}

		// Get the Message.getCode()
		JsonElement obj = mParser.parse(json);
		final int code = obj.getAsJsonObject().get("code").getAsInt();

		// Create the corresponding instance.
		switch (code) {
		case MSG_CHAT:
			msg = gson.fromJson(json, ChatMessage.class);
			break;
		case MSG_RAISE:
			msg = gson.fromJson(json, RaiseMessage.class);
			break;
		case MSG_FOLD:
			msg = gson.fromJson(json, FoldMessage.class);
			break;
		case MSG_CHECK:
			msg = gson.fromJson(json, CheckMessage.class);
			break;
		case MSG_CALL:
			msg = gson.fromJson(json, CallMessage.class);
			break;
		case MSG_ALLIN:
			msg = gson.fromJson(json, AllInMessage.class);
			break;
		case MSG_NEW:
			msg = gson.fromJson(json, NewPeerMessage.class);
			break;
		case MSG_DISCONNECT:
			msg = gson.fromJson(json, DisconnectMessage.class);
			break;
		case MSG_HANDSHAKE:
			msg = gson.fromJson(json, HandshakeMessage.class);
			break;
		case MSG_ROUNDEND:
			msg = gson.fromJson(json, RoundEndMessage.class);
			break;
		case MSG_GAMEEND:
			msg = gson.fromJson(json, GameEndMessage.class);
			break;
		case MSG_GAMESTART:
			msg = gson.fromJson(json, GameStartMessage.class);
			break;
		case MSG_NEXTTURN:
			msg = gson.fromJson(json, NextTurnMessage.class);
			break;
		case MSG_ADDCHIPS:
			msg = gson.fromJson(json, AddChipsMessage.class);
			break;
		default:
			break;
		}

		return msg;

	}

	// All The methods to create a diferent type of messages
	public static Message createRaiseMessage(double amount) {
		return new RaiseMessage(amount);
	}

	public static Message createChatMessage(String text) {
		return new ChatMessage(text);
	}

	public static Message createFoldMessage() {
		return new FoldMessage();
	}

	public static Message createCallMessage() {
		return new CallMessage();
	}

	public static Message createCheckMessage() {
		return new CheckMessage();
	}

	public static Message createAllInMessage() {
		return new AllInMessage();
	}

	public static Message createDisconnectMessage() {
		return new DisconnectMessage();
	}

}
